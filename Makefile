PROJECT=FNC
ARCH=${shell uname -p}
DATE=${shell date +"%Y-%m-%d"}
OUTFILE=${PROJECT}_${ARCH}_${DATE}

RUNHASKELL=runhaskell
SRCS=-isrc -ilib

run:
	./testHaskell.sh
	${RUNHASKELL} ${SRCS} src/Main.hs

buildInstall:
	ghc ${SRCS} src/Main.hs ; cp  src/Main bin/${OUTFILE}

clean:
	rm src/*.o src/*.hi src/Main 
	rm lib/*.o lib/*.hi 



