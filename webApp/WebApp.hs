{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
import           Control.Applicative ((<$>), (<*>))
import           Data.Text           (Text,unpack)
import           Data.Time           (Day)
import           Yesod
import           Yesod.Form.Jquery
import  Text.Hamlet (hamletFile)
import Data.Text.Encoding

import System.Posix.Redirect
import qualified Data.ByteString.Char8 as BT(unpack)

import Parser(parseFormulaFromString)
import FNC(procedure)
import Formulas as F (formulas,FC(..))
import Types (prettyPrint)

data App = App

mkYesod "App" [parseRoutes|
/ FormR GET
/fnc AnswerR POST
|]

instance Yesod App

instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

instance YesodJquery App

data PostDataFormula = PostDataFormula
    { formulaTA          :: Textarea }
  deriving Show


fncForm :: Html -> MForm Handler (FormResult PostDataFormula, Widget)
fncForm = renderDivs $ PostDataFormula
    <$> areq textareaField "Rentrez une formule : " Nothing
      
-- The GET handler displays the form
getFormR :: Handler Html
getFormR = do
    let formulas = map (prettyPrint . F.formula) F.formulas 
    -- Generate the form to be displayed
    (widget, enctype) <- generateFormPost fncForm
    defaultLayout $ do
        let title = "Calcul de la forme normale conjonctive"
        setTitle title
        [whamlet|
            <h2> #{title} 
            <p>
                <form method=post action=@{AnswerR} enctype=#{enctype}>
                   ^{widget}
                <p>
                <button>Submit
             <p> Des formules à tester :
               <ol>
                 $forall phi <- formulas
                   <li> #{phi}
        |]

postAnswerR :: Handler Html
postAnswerR = do
    ((result, widget), enctype) <- runFormPost fncForm
    case result of
        FormSuccess formula -> mainTreatementHtml formula
        _ -> defaultLayout $ do
                 let title="Résultat"
                 setTitle title
                 [whamlet|
                 <h2> #{title}
                <p>Invalid input, try again.
                <form method=post action=@{AnswerR} enctype=#{enctype}>
                    ^{widget}
                    <button>Submit
                    |]

mainTreatementIO :: PostDataFormula -> IO String
mainTreatementIO post =
  let
    formulaStr = unpack (unTextarea (formulaTA post))
  in
    case parseFormulaFromString formulaStr of
      Right f -> redirectStderr (procedure f) >>= \(s,_) -> return ((unpack . decodeUtf8) s)
      Left e -> return (show e)

mainTreatementHtml :: PostDataFormula -> Handler Html
mainTreatementHtml post = do
  result <- liftIO $ mainTreatementIO post
  defaultLayout $ do
    toWidgetHead [hamlet|
             <meta charset="UTF-8">
             |]
    toWidget [whamlet|
                        <p>
                          <div>
                            <code>
                              <pre>#{result}
                        <p>Retour au 
                            <a href=@{FormR}>formulaire
                        |]

--mainTemplate title content = $(widgetFile "templates/page.hamlet")

main :: IO ()
main = warp 3000 App
