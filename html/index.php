<?php

//$parameters = parse_ini_file('config.ini',true);
$string=file_get_contents('config.ini');
$parameters = parse_ini_string($string,true);
//print_r($parameters);
//exit();
if(!isset($parameters['cssFile'])){
    $parameters['cssFile']='style.css';
}

$result=validatePost($parameters);
switch(true){
    case ($result === true):
	$output=runCommand($parameters);
	break;
    case (gettype($result) === 'string'):
	$error=$result;
    default:
	$formInputs=buildFormInputs($parameters);
	break;
} 

function buildFormInputs($parameters){
    $inputSubmit="<input type='submit' name='submit' value='Executer' />\n";
    if(!isset($parameters['options'])){return $inputSubmit;}
    $formInputs = [];
    foreach($parameters['options'] as $optionName => $option){
	$type=isset($option['type'])?isset($option['type']):'';
	switch($type){
	    case 'textArea':
		$formInputs[$optionName]=sprintf(
		    "%s <textarea name='%s'/>%s</textarea>",
		    isset($option['label'])?$option['label']:ucfirst($optionName),
		    $optionName,
		    isset($option['value'])?htmlentities($option['value']):''
		);
		break;
	    default:
		$attributes='';
		if(isset($option['maxlength'])){
		    $attributes.=" maxlength=\"$option[maxlength]\"";
		    $attributes.=" size=\"$option[maxlength]\"";
		}
		$formInputs[$optionName]=sprintf(
		    "%s <input name='%s' type='text' value='%s' %s />",
		    isset($option['label'])?$option['label']:ucfirst($optionName),
		    $optionName,
		    isset($option['value'])?htmlentities($option['value']):'',
		    $attributes
		);
	}
    }
    $formInputs['submit']=$inputSubmit;
    return implode("<br />\n",$formInputs);
}

function validatePost(&$parameters){

    $isSubmitted=filter_input(INPUT_POST,'submit');
    if(!$isSubmitted){ return false; }
    
    foreach($parameters['options'] as $optionName => &$option){
	$value = filter_input(INPUT_POST,$optionName);
	if($value == null && isset($option['required'])
	   && $option['required']
	){
	    return sprintf(
		"%s champ requis",
		labelOfOption($optionName,$option)
	    );
	}
	if(isset($option['maxlength']) &&
	   strlen($value) > $option['maxlength']
	){
	    return sprintf(
		"Champ %s : trop long (max %s caractères)",
		labelOfOption($optionName,$option),
		$option['maxlength']
	    );
	}
	if($value){
	    $option['value'] = $value;
	}
    }
    return true;
}

function runCommand($parameters){
    if(isset($parameters['pathToBinary'])){
	try{
        $arch=trim(shell_exec('uname -p'));
        $precommand=__DIR__.'/'.$parameters['pathToBinary']."_$arch";
	    $command = realpath(__DIR__.'/'.$parameters['pathToBinary']."_$arch");
        if(!$command){
            $command = realpath(__DIR__.'/'.$parameters['pathToBinary']);
        }
	    foreach($parameters['options'] as $option => $data){
		if(isset($data['value'])){
		    $command.=" --$option=\"$data[value]\"";
		}
	    }
	    $result = shell_exec($command);
	    if($result===null){throw new Exception('result of shell_exec is null :' .$precommand);}
	    return $result; 
	}catch (Exception $e) {
	    return 'Could not execute command: ' . $e->getMessage();
	}
    }else{
	return 'Could not execute command';
    }
}

function setValue($name){
    global $$name;
    return isset($$name)?htmlentities($$name):'';
}

function labelOfOption($nameOfOption,$option){
    return isset($option['label'])?$option['label']:ucfirst($optionName);
}
?>
<!DOCTYPE html>
<html>
    <head>
	<title><?=$parameters['title']?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="<?=$parameters['cssFile']?>" />
    </head>
    <body>
	<h1><?=$parameters['title']?></h1>
	<?php if(isset($output)): ?>
	    <div id="result" class="container">
		<pre>
<?=setValue('output')?>
		</pre>
	    </div>
	    <hr />
	    <a href="">Retour</a>
	<?php else: ?>
	    <div style="text-align:center" >
		<?php if(isset($error)): ?>
		    <div class="error container"><?=setValue('error')?></div>
		<?php else: ?>
		<?php  endif ; ?>
		<form class="container" action="" method="POST">
		    <?=$formInputs?>
		</form>
		<?php if(isset($parameters['example'])): ?>
		    <?=$parameters['example']?>
		<?php endif ; ?>
	    </div>
	<?php endif ; ?>
    </body>
</html>
    
