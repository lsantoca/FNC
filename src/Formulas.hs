{- Ajoutez ici vos formules selon les exemples données -}

module Formulas where
import Types
import FNC

data FC =
  FC {
  formula :: Formula,
  comment :: String
  }

formulaDefault :: FC
formulaDefault = FC {
  formula = vrai,
  comment = "Pas de commetaire pour cette formule"
  }


formula1 = formulaDefault {
  formula = (non (p `et` (q `implique` (r `ou` s)))) `et` (p `ou` q)
  }
  
formula2 = formulaDefault {
  formula = (non (p `implique` q)) `ou` (non (q `implique`p))
  }
  
formula3 = formulaDefault {
  formula = ((q `ou` non p) `implique` (non  (non q) `ou`  non p)) `et`  non (( non  (non p) `ou`  non q) `et`(q `et` non p))
  }

{--
-- changed the biinplication to a simple implication
-- Exo TD3 2018 3.5
-- (p ⇒ ((q ∨ r) ∧ s)) ∧ ¬(q ⇔ (r ∧ (p ∨ s)))
formula4 =
  FC {
  formula =(p `implique` ((q `ou` r) `et` s)) `et` (non (q `biimplique` (r `et` (p `ou` s)))),
  comment = "Exo TD3 2018 3.5 : (p ⇒ ((q ∨ r) ∧ s)) ∧ ¬(q ⇔ (r ∧ (p ∨ s)))"
  }
--}

-- Exo TD3 2018 3.5
-- (p ⇒ ((q ∨ r) ∧ s)) ∧ ¬(q ⇔ (r ∧ (p ∨ s)))
formula4 =
  FC {
  formula =(p `implique` ((q `ou` r) `et` s)) `et` (non (q `implique` (r `et` (p `ou` s)))),
  comment = "Exo TD3 2018 3.5 : (p ⇒ ((q ∨ r) ∧ s)) ∧ ¬(q ⇒ (r ∧ (p ∨ s)))"
  }

-- Exo TD3 2018 3.6
-- ¬[(p⇒s)⇒((q⇒r)⇒((p∨q)⇒(s∧q∧r∧¬p)))]
formula5 = FC {
  formula = 
  non
  ((p`implique`s)`implique`((q`implique`r)`implique`((p `ou` q)`implique`(s`et`q`et`r`et` (non p))))),
  comment = "Exo TD3 2018 3.6 :  ¬[(p⇒s)⇒((q⇒r)⇒((p∨q)⇒(s∧q∧r∧¬p)))]"
  }

-- EXo TD 2018 3.4
-- 1.  (p∧¬((q∨r)⇒p))∨s;
-- 2. ψ2 = (p1 ∧q1)∨(p2 ∧q2);
-- 3. ψ3 =¬((p⇔q)⇒(r⇒s)).

formula6  = FC {
  formula = (p `et` (non ( (q `ou` r) `implique`p))) `ou` s,
  comment = "Exo TD3 2018 3.6.1 : (p∧¬((q∨r)⇒p))∨s;"
  }

formula7  = FC {
  formula = non ((p `ssi`q) `impl` (r `impl`s)),
  comment = "Exo TD3 2018 3.6.3 : ¬((p⇔q)⇒(r⇒s));"
  }


formula8 = FC {
  formula = non chi,
  comment = prettyPrint $ non chi
  }
  where
    chi =  (p `et` (p `impl` q)) `ou` ((r `impl` s) `impl` (((non s))  `impl` (non r)))  
-- 
formulas = [formula1,formula2,formula3,formula4,formula5,formula6,formula7,formula8]

