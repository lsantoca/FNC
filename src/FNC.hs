module FNC where
import MyTrace (annotate)
import System.IO (hPutStrLn,stderr)
import Types

    
elImplStep :: Formula -> Either Formula Formula

elImplStep f@(Atom x) = Left f

elImplStep f@(BO Impl phi psi) =
  case elImplStep phi of
    Right phi' -> Right (BO Impl phi' psi)
    Left _ -> case elImplStep psi of
      Right psi' -> Right (BO Impl phi psi')
      Left _ ->  Right (BO Disj (UO Neg phi) psi)

elImplStep f@(BO op phi psi) =
  case elImplStep phi of
    Right phi' -> Right (BO op phi' psi)
    Left _ -> case elImplStep psi of
      Right psi' -> Right (BO op phi psi')
      Left _ -> Left f

elImplStep f@(UO Neg phi) = case elImplStep phi of
  Right phi' -> Right (UO Neg phi')
  Left _ -> Left f 



pdNeg :: Formula -> Either Formula Formula
pdNeg f@(Atom _) = Left f
pdNeg f@(UO Neg (Atom _)) = Left f
pdNeg f@(BO op phi psi) = case pdNeg phi of
    Right phi' -> Right (BO op phi' psi)
    Left _ -> case pdNeg psi of
      Right psi' -> Right (BO op phi psi')
      Left _ -> Left f

pdNeg f@(UO Neg (UO Neg phi)) =
  case pdNeg phi of
    Right phi' -> Right (UO Neg (UO Neg phi'))
    Left _ -> Right phi

pdNeg f@(UO Neg (BO op phi psi)) =
  case pdNeg phi of
    Right phi' -> Right (UO Neg (BO op phi' psi))
    Left _ -> case pdNeg psi of
      Right psi' -> Right (UO Neg (BO op phi psi'))
      Left _ -> Right ((BO (opposite op) (UO Neg phi) (UO Neg psi)))

binDo :: (a -> Either a a) ->
         (a -> a -> a) ->
         a -> a -> Either a a
binDo f constr x y = case f x of
  Right x' -> Right (constr x' y)
  Left _ -> case f y of
    Right y' ->  Right (constr x y')
    Left _ -> Left (constr x y)


distrStep f@(Atom _) = Left f
distrStep f@(UO Neg _) = Left f
distrStep (BO Conj phi psi) = binDo distrStep (BO Conj) phi psi
distrStep (BO Disj phi@(BO Conj phi1 phi2) psi) =
  case distrStep phi of
    Right phi' -> Right (BO Disj phi' psi)
    Left _ -> case distrStep psi of
      Right psi' -> Right (BO Disj phi psi')
      Left _ -> Right (BO Conj (BO Disj phi1 psi) (BO Disj phi2 psi))
distrStep (BO Disj phi psi@(BO Conj psi1 psi2)) =
  case distrStep phi of
    Right phi' -> Right (BO Disj phi' psi)
    Left _ -> case distrStep psi of
      Right psi' -> Right (BO Disj phi psi')
      Left _ -> Right (BO Conj (BO Disj phi psi1) (BO Disj phi psi2))
distrStep (BO Disj phi psi) = binDo distrStep (BO Disj) phi psi


loopStep :: (Formula -> String) ->
            (Formula -> Either Formula Formula) ->
            Formula -> Either Formula Formula
loopStep howToShow stepfunction f = annotate (howToShow f)
  (stepfunction f >>= loopStep howToShow stepfunction )


eliminationImplication = annotate "\n1- Eliminer l'implication :"
  loopStep prettyPrint elImplStep
  
pushdownNegation = annotate "\n2- Pousser la negation vers les symboles propositionnels :"
  loopStep prettyPrint pdNeg 

distribute = annotate "\nPousser les disjonctions vers les litteraux :"
  loopStep prettyPrint distrStep 

procedure :: Formula -> IO ()
procedure f0 = do
  putStrLn $ "Formule : "++ prettyPrint f0
  case eliminationImplication f0 of
    Left f1 -> case pushdownNegation f1 of
      Left f2 -> case distribute f2 of
        Left _ -> putStr "\nFin de la procedure\n"

      

