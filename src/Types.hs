module Types where

type Prop = Int

data Binop = Conj | Disj | Impl deriving Show
opposite :: Binop -> Binop
opposite Conj = Disj
opposite Disj = Conj
data Unop = Neg deriving Show
data Formula = Atom Prop
             | BO Binop Formula Formula
             | UO Unop  Formula 
               deriving Show


{- printers -}
pp :: Binop -> String
pp Conj = " ^ "
pp Disj = " v "
pp Impl = " -> "

prettyPrint :: Formula -> String
prettyPrint (Atom x) = case x of
  0 -> "p"
  1 -> "q"
  2 -> "r"
  3 -> "s"
  _ ->  "p_" ++ show x 
prettyPrint (BO op phi psi) = "(" ++ prettyPrint phi ++ opstr ++ prettyPrint psi ++ ")"
  where
    opstr = pp op
prettyPrint (UO _ phi) = "~" ++ prettyPrint phi

{-shortcuyts -}

p,q,r,s::Formula
p = Atom 0
q = Atom 1
r = Atom 2
s = Atom 3

et,ou,impl,implique :: Formula -> Formula -> Formula
et = BO Conj
ou = BO Disj
impl = BO Impl
implique = impl
ssi phi psi = (phi `impl` psi) `et` (psi `impl` phi)

non :: Formula -> Formula
non = UO Neg

vrai,faux :: Formula
vrai = p `ou` non p
faux = p `et` non p



