import Control.Exception
import System.Environment
import Data.List (intercalate)
import Options.Applicative
import Data.Semigroup ((<>))

import Types
import FNC
import qualified Formulas as F (formulas,formula1,FC(..))
import Parser2(parseFormulaFromString)

data Options = Options {
  formula      :: String
  }

options :: Parser Options
options = Options
      <$> strOption
      (long "formula"
       <> metavar "FORMULA"
       <> value ""
       <> help "La formule propositionnelle à mettre en FNC" )

menuFormula :: F.FC -> String
menuFormula fc =
  "Formule  : " ++ prettyPrint (F.formula fc)
  ++"\nCommentaire : " ++ (F.comment fc)

menuFormulas :: [F.FC] -> String
menuFormulas formulas =
  let
    items = map (\n -> show n++") ") [1..length formulas]
    formulasStr = map menuFormula formulas
  in
    intercalate "\n" $ map (\(xs,ys) -> xs ++ ys) (zip items formulasStr)

menu = do
  putStrLn $ menuFormulas F.formulas
  putStr "Quelle formule : "
  str <- getLine
  return ((read str)::Int)

runWithMenu = do
  n <- menu
  let chosenFc = F.formulas !! (n -1)
  procedure (F.formula chosenFc)

main :: IO ()
main = execParser opts >>= runWithOptions
  where
    opts = info (options <**> helper)
      ( fullDesc
        <> progDesc "Demo de l'algorithme de mise en forme conjonctive"
        <> header "Mise en FNC" )

runWithOptions:: Options -> IO ()
runWithOptions opts =
  (
    if formula opts == ""  then runWithMenu
    else
      case parseFormulaFromString (formula opts) of
        Left e -> putStrLn $ "Error parsing the formula : " ++(show e)
        Right formula ->
--          putStrLn ("Your formula : " ++ prettyPrint formula)
--          >>
          procedure formula
  )`catchAny`  printError


catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

printError :: SomeException -> IO ()
printError e = do
        putStrLn $ "Erreur : " ++ show e
