module Lexer
    ( Token(..)
    , tokenize
    ) where

import Text.ParserCombinators.Parsec hiding (token, tokens)
import Control.Applicative ((<*), (*>), (<$>), (<*>))

data Token =
  PropVar Int
  | LPar
  | RPar
  | TConj
  | TDisj
  | TImpl
  | TNeg
  deriving (Show, Eq)



lpar, rpar, propvar, lop :: Parser Token
lpar = char '(' >> return LPar
rpar = char ')' >> return RPar

propvar =
  try (char 'p' >> many1 digit >>= \str -> return (PropVar ((read str)::Int) ))
  <|> 
  (char 'p' >> return (PropVar 0))
  <|>
  (char 'q' >> return (PropVar 1))
  <|>
  (char 'r' >> return (PropVar 2))
  <|>
  (char 's' >> return (PropVar 3))

lop = choice $
  map
  (\(str,tok) -> (string str >> return tok))
  [("^",TConj),("v",TDisj),("->",TImpl),("~",TNeg)]

token :: Parser Token
token = choice
    [ 
      lpar
    , rpar
    , lop
    , propvar
    ]

tokens :: Parser [Token]
tokens = spaces *> many (token <* spaces)

tokenize :: SourceName -> String -> Either ParseError [Token]
tokenize = runParser tokens ()
