module Parser where

import Types as Formulas
import Lexer

import Text.Parsec as P

type Parser a = Parsec [Token] () a

advance :: SourcePos -> t -> [Token] -> SourcePos
advance pos _ (tok:_) = pos
advance pos _ [] = pos


satisfy :: (Token -> Bool) -> Parser Token
satisfy f = tokenPrim show
                      advance
                      (\c -> if f c then Just c else Nothing)

tok :: Token -> Parser Token
tok t = (Parser.satisfy $(== t)) <?> show t

formula = do
  phi <- formulaP
  -- notFollowedBy (choice (map tok [TConj,TDisj,LPar,RPar]))
  return phi

formulaP = -- try implication <|>
  implication

parensEN :: Parser Formula -> Parser Formula
parensEN parser = do
    tok LPar
    phi <- parser
    tok RPar
    return phi

term :: Parser Formula -> Parser Formula
term parser = propSymbolP <|> negation <|> parensEN parser

longOPl,longOPr :: Token ->
           (Formula->Formula->Formula)
           -> Parser Formula
           -> Parser Formula
longOPl token op next =
  let
    parser' = tok token >> return op
    term' = term next
  in
    chainl1 term' parser'
      
longOPr token op next = chainr1 term' parser'
  where
    parser' = tok token >> return op
    term' = term next

conjunction = longOPl TConj et (try implication <|> try disjunction <|> conjunction)
disjunction = longOPl TDisj ou (try conjunction <|> try implication <|> disjunction)
implication = longOPr TImpl impl (try disjunction <|> conjunction <|> implication)

negation = tok TNeg >> formulaP >>= \phi -> return (non phi)
propSymbolP :: Parser Formula
propSymbolP = tokenPrim show advance toAtomFormula
  where
    toAtomFormula (PropVar x) = Just (Atom x)
    toAtomFormula _ = Nothing


    
parseFormulaFromString :: String -> Either ParseError Formula
parseFormulaFromString string = do
    streamOfTokens <- tokenize "" string
    runParser formula () "" streamOfTokens

{-
parseFile :: String -> IO Formula
parseFile file =
   do program  <- readFile file
      case parse formulaP "" program of
        Left e  -> print e >> fail "parse error"
        Right r -> return r

constant :: Parser Formula
constant =
  (string " true " >> return Formulas.true)
  <| >
  (string " false " >> return Formulas.false)
-}
