module Parser2 where
import Types

--import System.IO
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

languageDef =
   emptyDef { 
  Token.identStart      = oneOf "pqrs",
  Token.identLetter     = digit,
  Token.reservedOpNames = ["^", "v", "~", "->"]
  }

lexer = Token.makeTokenParser languageDef
identifier = Token.identifier lexer -- parses an identifier
reservedOp = Token.reservedOp lexer -- parses an operator
parens     = Token.parens     lexer -- parses surrounding parenthesis:
                                     --   parens p
                                     -- takes care of the parenthesis and
                                     -- uses p to parse what's inside them
whiteSpace = Token.whiteSpace lexer -- parses whitespace

{--
Following
https://www.cs.unm.edu/~mccune/mace4/manual/2009-11A/syntax.html#built_in
We have
~ ^  ->  v
--}

operators = [
  [Prefix (reservedOp "~"   >> return non)]
  ,
  [Infix  (reservedOp "^"   >> return et) AssocLeft]
  ,
  [Infix  (reservedOp "->"   >> return impl) AssocLeft]
  ,
  [Infix  (reservedOp "v"   >> return ou) AssocLeft]
  ]
               

propSymbolP :: Parser Formula
propSymbolP = do
  string <- identifier
  case string of
    'p':[] -> return (Atom 0)
    'p':digits -> return (Atom ((read digits)::Int))
    'q':[] -> return (Atom 1)
    'r':[] -> return (Atom 2)
    's':[] -> return (Atom 3)
    _ -> fail "La syntaxe pour les symboles propositionnels n'est pas correcte"

formulaParser,formulaP :: Parser Formula
formulaParser = whiteSpace >> formulaP
formulaP = buildExpressionParser operators term <?> "formula"
term = parens formulaP  <|> propSymbolP <?> "term"

parseFormulaFromString :: String -> Either ParseError Formula
parseFormulaFromString string = parse formulaParser "" string 

